#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include "StandardIncludes.h"
#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"
#include "SkyBox.h"


class GameController: public Singleton<GameController>
{
public:
	//Constructor / Destructors
	GameController();
	virtual ~GameController();

	//Methods
	void Initialize();
	void RunGame();
	void RotateFighter(GLFWwindow* window);
private:
	Shader m_shaderColor;
	Shader m_shaderDiffuse;
	Shader m_shaderFont;
	Camera m_camera;
	vector<Mesh> m_meshBoxes;
	Shader m_shaderSkybox;
	SkyBox m_skybox;
	GLuint m_vao;

};
#endif //GAMECONTROLLER_H

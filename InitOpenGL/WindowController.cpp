#include "WindowController.h"

//Contructors / Destructors
WindowController::WindowController()
{
	m_window = nullptr;

}

WindowController::~WindowController()
{
	if (m_window != nullptr)
	{
		glfwTerminate();
		m_window = nullptr;
	}
}

Resolution WindowController::GetResolution()
{
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	return Resolution(mode->width, mode->height);
}
void WindowController::NewWindow()
{
	M_ASSERT(glfwInit(), "Failed to initialize GLFW");

	// Open a window and create its OpenGL context
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	M_ASSERT((m_window = glfwCreateWindow(1600, 900, "Final Proj", nullptr, nullptr)) != nullptr, "FAILED TO OPEN GLFW WINDOW");
	glfwMakeContextCurrent(m_window);
}

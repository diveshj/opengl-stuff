#ifndef MESH_H
#define MESH_H

#include "StandardIncludes.h"
#include "Texture.h"
#include "ase_loader.h"

class Shader;

class Mesh
{
public:
	//contructors / destructors
	Mesh();
	virtual ~Mesh();

	//Accessors
	void SetPosition(glm::vec3 _position) { m_position = _position; }
	glm::vec3 GetPosition() { return m_position; }
	glm::vec3 GetRotation() { return m_rotation; }
	glm::vec3 GetScale() { return m_scale; }
	void SetScale(glm::vec3 _scale) { m_scale = _scale; }
	void Scale(glm::vec3 _scale) { m_scale += _scale; }
	void SetLightPosition(glm::vec3 _lightPosition) { m_lightPosition = _lightPosition; }
	void SetRotation(glm::vec3 _rotation) { m_rotation = _rotation; }
	void Rotate(glm::vec3 _rotation) { m_rotation += _rotation; }
	void SetLightColor(glm::vec3 _lightColor) { m_lightColor = _lightColor; }
	void SetCameraPosition(glm::vec3 _cameraPosition) { m_cameraPosition = _cameraPosition; }
	void SetColor(glm::vec3 _color) { m_color = _color; }
	glm::vec3 GetColor() { return m_color; }


	//Methods
	void Create(Shader* _shader, string _file, int _instanceCount = 1);
	void Cleanup();
	void Render(glm::mat4 _wvp, glm::vec3 _specColorVal, float _specStrength);
	void CalculateTransform();
	string RemoveFolder(string _map);

	//members
	static vector<Mesh> Lights;
	bool customRotation;


private:
	//Methods
	void SetShaderVariables(glm::mat4 _pv, glm::vec3 _specColorVal, float _specStrength);
	void BindAttributes();
	string Concat(string _s1, int _index, string _s2);
	void CalculateTangents(vector < asel::Vertex> _vertices, asel::Vector3& _tangent, asel::Vector3& _bitangent);

	Shader* m_shader;
	Texture m_textureDiffuse;
	Texture m_textureSpecular;
	Texture m_textureNormal;
	GLuint m_vertexBuffer;	//GPU Buffer
	GLuint m_indexBuffer;	//GPU Buffer
	GLuint m_instanceBuffer; //GPU Buffer

	vector<GLfloat> m_vertexData;
	vector<GLubyte> m_indexData;
	vector<GLfloat>m_instanceData;
	bool m_enableNormalMap;
	int m_elementSize;
	int m_instanceCount;
	bool m_enableInstancing;


	//Transform
	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;
	glm::mat4 m_world;
	
	//Lights
	glm::vec3 m_lightPosition;
	glm::vec3 m_lightColor;
	glm::vec3 m_cameraPosition;
	glm::vec3 m_color;
};

#endif //MESH_H
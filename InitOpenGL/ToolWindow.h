#pragma once

namespace InitOpenGL {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ToolWindow
	/// </summary>
	public ref class ToolWindow : public System::Windows::Forms::Form
	{
	public:
		static bool lightMove;
		static bool resetLightPosBool;
		static int specStr;
		static int specValR;
		static int specValG;
		static int specValB;

		static bool colorbyPosBool;
		static bool resetMeshPosBool;

		static bool transformBool;
		static bool resetTransformBool;
		static bool translateMeshBool;
		static bool rotateMeshBool;
		static bool scaleMeshBool;

		static bool spaceScene;

	private: System::Windows::Forms::RadioButton^ moveLight;
	private: System::Windows::Forms::Button^ resetLight;
	private: System::Windows::Forms::TrackBar^ specStrengthVal;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ specStrSliderLabel;
	private: System::Windows::Forms::TrackBar^ SpecValR;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TrackBar^ SpecValG;
	private: System::Windows::Forms::TrackBar^ SpecValB;
	private: System::Windows::Forms::Label^ SpecValRText;
	private: System::Windows::Forms::Label^ SpecValGText;
	private: System::Windows::Forms::Label^ SpecValBText;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::RadioButton^ colorByPosRadio;
	private: System::Windows::Forms::Button^ resetMeshPos;
	private: System::Windows::Forms::RadioButton^ TransformRadio;
	private: System::Windows::Forms::Button^ resetTransformBut;
	private: System::Windows::Forms::CheckBox^ translateBool;
	private: System::Windows::Forms::CheckBox^ rotateBool;
	private: System::Windows::Forms::CheckBox^ scaleBool;
	private: System::Windows::Forms::RadioButton^ spaceSceneRadio;


	public:ToolWindow(void)
		{
			InitializeComponent();

			lightMove = moveLight->Checked;
			resetLightPosBool;

			specStr = specStrengthVal->Value;
			specStrSliderLabel->Text = specStrengthVal->Value.ToString();
			specValR = SpecValR->Value;
			SpecValRText->Text = (SpecValR->Value).ToString();
			specValG = SpecValG->Value;
			SpecValGText->Text = (SpecValG->Value).ToString();
			specValB = SpecValB->Value;
			SpecValBText->Text = (SpecValB->Value).ToString();

			colorbyPosBool = false;
			resetMeshPosBool = false;

			transformBool = false;;
			resetTransformBool = false;
			translateMeshBool = false;
			rotateMeshBool = false;
			scaleMeshBool = false;

			spaceScene = false;

		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ToolWindow()
		{
			if (components)
			{
				delete components;
			}
		}




	protected:



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->moveLight = (gcnew System::Windows::Forms::RadioButton());
			this->resetLight = (gcnew System::Windows::Forms::Button());
			this->specStrengthVal = (gcnew System::Windows::Forms::TrackBar());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->specStrSliderLabel = (gcnew System::Windows::Forms::Label());
			this->SpecValR = (gcnew System::Windows::Forms::TrackBar());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SpecValG = (gcnew System::Windows::Forms::TrackBar());
			this->SpecValB = (gcnew System::Windows::Forms::TrackBar());
			this->SpecValRText = (gcnew System::Windows::Forms::Label());
			this->SpecValGText = (gcnew System::Windows::Forms::Label());
			this->SpecValBText = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->colorByPosRadio = (gcnew System::Windows::Forms::RadioButton());
			this->resetMeshPos = (gcnew System::Windows::Forms::Button());
			this->TransformRadio = (gcnew System::Windows::Forms::RadioButton());
			this->resetTransformBut = (gcnew System::Windows::Forms::Button());
			this->translateBool = (gcnew System::Windows::Forms::CheckBox());
			this->rotateBool = (gcnew System::Windows::Forms::CheckBox());
			this->scaleBool = (gcnew System::Windows::Forms::CheckBox());
			this->spaceSceneRadio = (gcnew System::Windows::Forms::RadioButton());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->specStrengthVal))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpecValR))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpecValG))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpecValB))->BeginInit();
			this->SuspendLayout();
			// 
			// moveLight
			// 
			this->moveLight->Checked = true;
			this->moveLight->Location = System::Drawing::Point(0, 0);
			this->moveLight->Name = L"moveLight";
			this->moveLight->Size = System::Drawing::Size(104, 24);
			this->moveLight->TabIndex = 25;
			this->moveLight->TabStop = true;
			this->moveLight->Text = L"Move Light";
			this->moveLight->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::moveLight_CheckedChanged);
			// 
			// resetLight
			// 
			this->resetLight->Location = System::Drawing::Point(45, 37);
			this->resetLight->Name = L"resetLight";
			this->resetLight->Size = System::Drawing::Size(129, 23);
			this->resetLight->TabIndex = 1;
			this->resetLight->Text = L"Reset Light Position";
			this->resetLight->UseVisualStyleBackColor = true;
			this->resetLight->Click += gcnew System::EventHandler(this, &ToolWindow::resetLight_Click);
			// 
			// specStrengthVal
			// 
			this->specStrengthVal->Location = System::Drawing::Point(125, 67);
			this->specStrengthVal->Maximum = 128;
			this->specStrengthVal->Minimum = 1;
			this->specStrengthVal->Name = L"specStrengthVal";
			this->specStrengthVal->Size = System::Drawing::Size(307, 45);
			this->specStrengthVal->TabIndex = 2;
			this->specStrengthVal->Value = 4;
			this->specStrengthVal->Scroll += gcnew System::EventHandler(this, &ToolWindow::specStrengthVal_Scroll);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(11, 76);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(92, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Specular Strength";
			// 
			// specStrSliderLabel
			// 
			this->specStrSliderLabel->AutoSize = true;
			this->specStrSliderLabel->Location = System::Drawing::Point(439, 76);
			this->specStrSliderLabel->Name = L"specStrSliderLabel";
			this->specStrSliderLabel->Size = System::Drawing::Size(13, 13);
			this->specStrSliderLabel->TabIndex = 4;
			this->specStrSliderLabel->Text = L"4";
			// 
			// SpecValR
			// 
			this->SpecValR->Location = System::Drawing::Point(125, 118);
			this->SpecValR->Maximum = 300;
			this->SpecValR->Name = L"SpecValR";
			this->SpecValR->Size = System::Drawing::Size(307, 45);
			this->SpecValR->TabIndex = 5;
			this->SpecValR->Value = 3;
			this->SpecValR->Scroll += gcnew System::EventHandler(this, &ToolWindow::SpecValR_Scroll);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(14, 169);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(76, 13);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Specular Color";
			// 
			// SpecValG
			// 
			this->SpecValG->Location = System::Drawing::Point(125, 158);
			this->SpecValG->Maximum = 300;
			this->SpecValG->Name = L"SpecValG";
			this->SpecValG->Size = System::Drawing::Size(307, 45);
			this->SpecValG->TabIndex = 9;
			this->SpecValG->Value = 3;
			this->SpecValG->Scroll += gcnew System::EventHandler(this, &ToolWindow::SpecValG_Scroll);
			// 
			// SpecValB
			// 
			this->SpecValB->Location = System::Drawing::Point(125, 198);
			this->SpecValB->Maximum = 300;
			this->SpecValB->Name = L"SpecValB";
			this->SpecValB->Size = System::Drawing::Size(307, 45);
			this->SpecValB->TabIndex = 10;
			this->SpecValB->Value = 3;
			this->SpecValB->Scroll += gcnew System::EventHandler(this, &ToolWindow::SpecValB_Scroll);
			// 
			// SpecValRText
			// 
			this->SpecValRText->AutoSize = true;
			this->SpecValRText->Location = System::Drawing::Point(438, 128);
			this->SpecValRText->Name = L"SpecValRText";
			this->SpecValRText->Size = System::Drawing::Size(22, 13);
			this->SpecValRText->TabIndex = 11;
			this->SpecValRText->Text = L"1.0";
			// 
			// SpecValGText
			// 
			this->SpecValGText->AutoSize = true;
			this->SpecValGText->Location = System::Drawing::Point(438, 169);
			this->SpecValGText->Name = L"SpecValGText";
			this->SpecValGText->Size = System::Drawing::Size(22, 13);
			this->SpecValGText->TabIndex = 12;
			this->SpecValGText->Text = L"1.0";
			// 
			// SpecValBText
			// 
			this->SpecValBText->AutoSize = true;
			this->SpecValBText->Location = System::Drawing::Point(438, 206);
			this->SpecValBText->Name = L"SpecValBText";
			this->SpecValBText->Size = System::Drawing::Size(22, 13);
			this->SpecValBText->TabIndex = 13;
			this->SpecValBText->Text = L"1.0";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(96, 128);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(15, 13);
			this->label3->TabIndex = 14;
			this->label3->Text = L"R";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(96, 169);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(15, 13);
			this->label4->TabIndex = 15;
			this->label4->Text = L"G";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(96, 206);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(14, 13);
			this->label5->TabIndex = 16;
			this->label5->Text = L"B";
			// 
			// colorByPosRadio
			// 
			this->colorByPosRadio->AutoSize = true;
			this->colorByPosRadio->Location = System::Drawing::Point(6, 235);
			this->colorByPosRadio->Name = L"colorByPosRadio";
			this->colorByPosRadio->Size = System::Drawing::Size(104, 17);
			this->colorByPosRadio->TabIndex = 17;
			this->colorByPosRadio->Text = L"Color By Position";
			this->colorByPosRadio->UseVisualStyleBackColor = true;
			this->colorByPosRadio->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::colorByPosRadio_CheckedChanged);
			// 
			// resetMeshPos
			// 
			this->resetMeshPos->Location = System::Drawing::Point(35, 258);
			this->resetMeshPos->Name = L"resetMeshPos";
			this->resetMeshPos->Size = System::Drawing::Size(129, 23);
			this->resetMeshPos->TabIndex = 18;
			this->resetMeshPos->Text = L"Reset Mesh Position";
			this->resetMeshPos->UseVisualStyleBackColor = true;
			this->resetMeshPos->Click += gcnew System::EventHandler(this, &ToolWindow::resetMeshPos_Click);
			// 
			// TransformRadio
			// 
			this->TransformRadio->AutoSize = true;
			this->TransformRadio->Location = System::Drawing::Point(6, 287);
			this->TransformRadio->Name = L"TransformRadio";
			this->TransformRadio->Size = System::Drawing::Size(72, 17);
			this->TransformRadio->TabIndex = 19;
			this->TransformRadio->Text = L"Transform";
			this->TransformRadio->UseVisualStyleBackColor = true;
			this->TransformRadio->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::TransformRadio_CheckedChanged);
			// 
			// resetTransformBut
			// 
			this->resetTransformBut->Location = System::Drawing::Point(35, 310);
			this->resetTransformBut->Name = L"resetTransformBut";
			this->resetTransformBut->Size = System::Drawing::Size(129, 23);
			this->resetTransformBut->TabIndex = 20;
			this->resetTransformBut->Text = L"Reset Transform";
			this->resetTransformBut->UseVisualStyleBackColor = true;
			this->resetTransformBut->Click += gcnew System::EventHandler(this, &ToolWindow::resetTransformBut_Click);
			// 
			// translateBool
			// 
			this->translateBool->AutoSize = true;
			this->translateBool->Location = System::Drawing::Point(45, 340);
			this->translateBool->Name = L"translateBool";
			this->translateBool->Size = System::Drawing::Size(70, 17);
			this->translateBool->TabIndex = 21;
			this->translateBool->Text = L"Translate";
			this->translateBool->UseVisualStyleBackColor = true;
			this->translateBool->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::translateBool_CheckedChanged);
			// 
			// rotateBool
			// 
			this->rotateBool->AutoSize = true;
			this->rotateBool->Location = System::Drawing::Point(45, 363);
			this->rotateBool->Name = L"rotateBool";
			this->rotateBool->Size = System::Drawing::Size(58, 17);
			this->rotateBool->TabIndex = 22;
			this->rotateBool->Text = L"Rotate";
			this->rotateBool->UseVisualStyleBackColor = true;
			this->rotateBool->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::rotateBool_CheckedChanged);
			// 
			// scaleBool
			// 
			this->scaleBool->AutoSize = true;
			this->scaleBool->Location = System::Drawing::Point(45, 386);
			this->scaleBool->Name = L"scaleBool";
			this->scaleBool->Size = System::Drawing::Size(53, 17);
			this->scaleBool->TabIndex = 23;
			this->scaleBool->Text = L"Scale";
			this->scaleBool->UseVisualStyleBackColor = true;
			this->scaleBool->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::scaleBool_CheckedChanged);
			// 
			// spaceSceneRadio
			// 
			this->spaceSceneRadio->AutoSize = true;
			this->spaceSceneRadio->Location = System::Drawing::Point(6, 409);
			this->spaceSceneRadio->Name = L"spaceSceneRadio";
			this->spaceSceneRadio->Size = System::Drawing::Size(90, 17);
			this->spaceSceneRadio->TabIndex = 24;
			this->spaceSceneRadio->Text = L"Space Scene";
			this->spaceSceneRadio->UseVisualStyleBackColor = true;
			this->spaceSceneRadio->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::spaceSceneRadio_CheckedChanged);
			// 
			// ToolWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(469, 461);
			this->Controls->Add(this->spaceSceneRadio);
			this->Controls->Add(this->scaleBool);
			this->Controls->Add(this->rotateBool);
			this->Controls->Add(this->translateBool);
			this->Controls->Add(this->resetTransformBut);
			this->Controls->Add(this->TransformRadio);
			this->Controls->Add(this->resetMeshPos);
			this->Controls->Add(this->colorByPosRadio);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->SpecValBText);
			this->Controls->Add(this->SpecValGText);
			this->Controls->Add(this->SpecValRText);
			this->Controls->Add(this->SpecValB);
			this->Controls->Add(this->SpecValG);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->SpecValR);
			this->Controls->Add(this->specStrSliderLabel);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->specStrengthVal);
			this->Controls->Add(this->resetLight);
			this->Controls->Add(this->moveLight);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"ToolWindow";
			this->Text = L"Tool Window";
			this->TopMost = true;
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->specStrengthVal))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpecValR))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpecValG))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpecValB))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		private: System::Void moveLight_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
		{
			lightMove = moveLight->Checked;
		}
private: System::Void resetLight_Click(System::Object^ sender, System::EventArgs^ e) 
{
	resetLightPosBool = true;
}
private: System::Void specStrengthVal_Scroll(System::Object^ sender, System::EventArgs^ e) 
{
	specStr = specStrengthVal->Value;
	specStrSliderLabel->Text = specStrengthVal->Value.ToString();
}
private: System::Void SpecValR_Scroll(System::Object^ sender, System::EventArgs^ e) 
{
	specValR = SpecValR->Value;
	SpecValRText->Text = SpecValR->Value.ToString();
}
private: System::Void SpecValG_Scroll(System::Object^ sender, System::EventArgs^ e) 
{
	specValG = SpecValG->Value;
	SpecValGText->Text = SpecValG->Value.ToString();
}
private: System::Void SpecValB_Scroll(System::Object^ sender, System::EventArgs^ e) 
{
	specValB = SpecValB->Value;
	SpecValBText->Text = SpecValB->Value.ToString();
}
private: System::Void colorByPosRadio_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
	colorbyPosBool = colorByPosRadio->Checked;
}
private: System::Void resetMeshPos_Click(System::Object^ sender, System::EventArgs^ e) 
{
	resetMeshPosBool = true;
}
private: System::Void TransformRadio_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
	transformBool = true;
}
private: System::Void resetTransformBut_Click(System::Object^ sender, System::EventArgs^ e) 
{
	resetTransformBool = true;
}
private: System::Void translateBool_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
	translateMeshBool = translateBool->Checked;
}
private: System::Void rotateBool_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
	rotateMeshBool = rotateBool->Checked;
}
private: System::Void scaleBool_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
	scaleMeshBool = scaleBool->Checked;
}
private: System::Void spaceSceneRadio_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
{
	spaceScene = spaceSceneRadio->Checked;
}

};
}

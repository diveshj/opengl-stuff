#version 330

in vec3 retNormal;
in vec2 retTexCoord;
in vec3 retFragPos;
in vec3 retViewDirection;
in mat4 retWorld;
in mat3 retTBN;

out vec4 FragColor;

struct Material
{
	float specularStrength;
	sampler2D diffuseTexture;
	sampler2D specularTexture;
	sampler2D normalTexture;
};

struct Light
{
	vec3 position;
	vec3 direction;
	vec3 color;
	vec3 specularColor;
	vec3 ambientColor;
	vec3 diffuseColor;

	float constant;
	float linear;
	float quadratic;

	float coneAngle;
	float falloff;

};

uniform Material material;
#define NR_LIGHTS 4
uniform Light light[NR_LIGHTS];
uniform bool EnableNormalMap = false;


void main()
{
	//Early out if we have no color at this fragment position
	vec4 diffColor = texture(material.diffuseTexture, retTexCoord);
	if (diffColor.a == 0)
	{
		discard;
	}

vec3 finalColor = vec3(0);
for (int i =0; i <NR_LIGHTS; i++)
{

	vec3 lightDir = normalize(light[i].position - retFragPos);
	vec3 normal = retNormal;
	if(EnableNormalMap == true)
	{
		  normal = texture(material.normalTexture, retTexCoord).rgb;
		  normal = normal * 2.0f - 1.0f;
		  normal = normalize(retTBN * normal);
	}

	vec3 lambertianStrength = dot(normal, lightDir) * light[i].color;
	vec3 refl = reflect(-lightDir, normal);
	float specularStrength = pow(max(dot(refl, retViewDirection), 0.0f), material.specularStrength);

	vec3 ambient = diffColor.rgb * light[i].ambientColor / NR_LIGHTS;
	vec3 lambertian = lambertianStrength * diffColor.rgb * light[i].diffuseColor;
	vec3 specular = specularStrength * texture(material.specularTexture, retTexCoord).rgb * light[i].specularColor;
		finalColor += ambient + lambertian + specular;

	}
	FragColor = vec4(finalColor, diffColor.a);
}
#include "Mesh.h"
#include "Shader.h"
#include "ase_loader.h"
#include <random>
vector<Mesh> Mesh::Lights;

Mesh::Mesh()
{
	m_shader = nullptr;
	m_textureDiffuse = {};
	m_textureSpecular = {};
	m_textureNormal = {};
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_position = { 0,0,0 };
	m_rotation = { 0,0,0 };
	m_scale = { 1,1,1 };
	m_world = glm::mat4();
	m_lightPosition = { 0,0,0 };
	m_lightColor = { 1,1,1 };
	m_enableNormalMap = false;
	m_instanceCount = 1;
	m_enableInstancing = false;
	m_elementSize = 0;
	customRotation = false;
}
Mesh::~Mesh()
{

}


void Mesh::Create(Shader* _shader, string _file, int _instanceCount)
{
	#pragma region  ASE LOADER

	m_shader = _shader;
	m_instanceCount = _instanceCount;
	if (m_instanceCount > 1)
	{
		m_enableInstancing = true;
	}
	asel::Loader Loader;
	M_ASSERT(Loader.LoadFile(_file) == true, "Failed to load Mesh.");

	for (unsigned int i = 0; i < Loader.LoadedMeshes.size(); i++)
	{
		asel::Mesh curMesh = Loader.LoadedMeshes[i];
		vector<asel::Vector3> tangents;
		vector<asel::Vector3> bitangents;
		vector<asel::Vertex> triangles;
		asel::Vector3 tangent;
		asel::Vector3 bitangent;


		for (unsigned int j = 0; j < curMesh.Vertices.size(); j += 3)
		{
			triangles.clear();
			triangles.push_back(curMesh.Vertices[j]);
			triangles.push_back(curMesh.Vertices[j + 1]);
			triangles.push_back(curMesh.Vertices[j + 2]);
			CalculateTangents(triangles, tangent, bitangent);
			tangents.push_back(tangent);
			bitangents.push_back(bitangent);
		}

		for (unsigned int j = 0; j < curMesh.Vertices.size(); j++)
		{
			m_vertexData.push_back(curMesh.Vertices[j].Position.X);
			m_vertexData.push_back(curMesh.Vertices[j].Position.Y);
			m_vertexData.push_back(curMesh.Vertices[j].Position.Z);

			m_vertexData.push_back(curMesh.Vertices[j].Normal.X);
			m_vertexData.push_back(curMesh.Vertices[j].Normal.Y);
			m_vertexData.push_back(curMesh.Vertices[j].Normal.Z);

			m_vertexData.push_back(curMesh.Vertices[j].TextureCoordinate.X);
			m_vertexData.push_back(curMesh.Vertices[j].TextureCoordinate.Y);

			if (Loader.LoadedMaterials[0].normalMap != "")
			{
				int index = j / 3;
				m_vertexData.push_back(tangents[index].X);
				m_vertexData.push_back(tangents[index].Y);
				m_vertexData.push_back(tangents[index].Z);

				m_vertexData.push_back(bitangents[index].X);
				m_vertexData.push_back(bitangents[index].Y);
				m_vertexData.push_back(bitangents[index].Z);
			}
		}
	}

	m_textureDiffuse = Texture();
	m_textureDiffuse.LoadTexture("../Assets/Textures/" + RemoveFolder(Loader.LoadedMaterials[0].diffuseMap));

	m_textureSpecular = Texture();
	if (Loader.LoadedMaterials[0].specularMap != "")
	{
		m_textureSpecular.LoadTexture("../Assets/Textures/" + RemoveFolder(Loader.LoadedMaterials[0].specularMap));

	}

	m_textureNormal = Texture();
	if (Loader.LoadedMaterials[0].normalMap != "")
	{
		m_textureNormal.LoadTexture("../Assets/Textures/" + RemoveFolder(Loader.LoadedMaterials[0].normalMap));
		m_enableNormalMap = true;
	}
	glGenBuffers(1, &m_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, m_vertexData.size() * sizeof(float), m_vertexData.data(), GL_STATIC_DRAW);


#pragma endregion


	//Instancing and changing their positions
	if (m_enableInstancing)
	{
		glGenBuffers(1, &m_instanceBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_instanceBuffer);


		const int x_MIN = -1000;
		const int x_MAX = 1000;

		std::random_device rd;
		std::default_random_engine eng(rd());
		std::uniform_int_distribution<> distrx(x_MIN, x_MAX);


		for (unsigned int i = 0; i < m_instanceCount; i++)
		{
			int rx = distrx(eng);
			int ry = distrx(eng);
			int rz = distrx(eng);
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(rx, ry, rz));
			model = glm::scale(model, glm::vec3(1 + rand() % 4, 1 + rand() % 4, 1 + rand() % 4));



			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					m_instanceData.push_back(model[x][y]);

				}
			}
		}

		//Bind for instancing
		glBufferData(GL_ARRAY_BUFFER, m_instanceCount * sizeof(glm::mat4), m_instanceData.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

}

void Mesh::Cleanup()
{
	glDeleteBuffers(1, &m_indexBuffer);
	glDeleteBuffers(1, &m_vertexBuffer);
	m_textureDiffuse.Cleanup();
	m_textureSpecular.Cleanup();
	m_textureNormal.Cleanup();
}

void Mesh::CalculateTransform()
{
	m_world = glm::translate(glm::mat4(1.0f), m_position);
	m_world = glm::rotate(m_world, m_rotation.x, glm::vec3(1, 0, 0));
	m_world = glm::rotate(m_world, m_rotation.y, glm::vec3(0, 1, 0));
	m_world = glm::rotate(m_world, m_rotation.z, glm::vec3(0, 0, 1));
	m_world = glm::scale(m_world, m_scale);
}

string Mesh::RemoveFolder(string _map)
{
	const size_t last_slash_idx = _map.find_last_of("\\");
	if (std::string::npos != last_slash_idx)
	{
		_map.erase(0, last_slash_idx + 1);
	}

	return _map;
}

void Mesh::SetShaderVariables(glm::mat4 _pv, glm::vec3 _specColorVal, float _specStrength)
{
	//Camera/World
	m_shader->setMat4("World", m_world);
	m_shader->setMat4("WVP", _pv * m_world);
	m_shader->setVec3("CameraPosition", m_cameraPosition);
	m_shader->setInt("EnableNormalMap", m_enableNormalMap);
	m_shader->setInt("EnableInstancing", m_enableInstancing);
	//Lights
	for (unsigned int i = 0; i < Lights.size(); i++)
	{
		m_shader->setVec3(Concat("light[", i, "].position").c_str(), Lights[i].GetPosition());
		m_shader->setVec3(Concat("light[", i, "].color").c_str(), m_lightColor);

		m_shader->SetFloat(Concat("light[", i, "].constant").c_str(), 1.0f);
		m_shader->SetFloat(Concat("light[", i, "].linear").c_str(), 0.09f);
		m_shader->SetFloat(Concat("light[", i, "].quadratic").c_str(), 0.032f);

		m_shader->setVec3(Concat("light[", i, "].ambientColor").c_str(), { 0.1f,0.1f,0.1f });
		m_shader->setVec3(Concat("light[", i, "].diffuseColor").c_str(), Lights[i].GetColor());
		m_shader->setVec3(Concat("light[", i, "].specularColor").c_str(), { _specColorVal});

		m_shader->setVec3(Concat("light[", i, "].direction").c_str(), glm::normalize(glm::vec3({ 0.0f + i * 0.1f ,0.0f, 0.0f + i * 0.1f }) - Lights[i].GetPosition()));
		m_shader->SetFloat(Concat("light[", i, "].coneAngle").c_str(), glm::radians(5.0f));
		m_shader->SetFloat(Concat("light[", i, "].falloff").c_str(), 200);
	}

	m_shader->SetFloat("material.specularStrength", _specStrength);
	m_shader->SetTextureSampler("material.diffuseTexture", GL_TEXTURE0, 0, m_textureDiffuse.GetTexture());
	m_shader->SetTextureSampler("material.specularTexture", GL_TEXTURE1, 1, m_textureSpecular.GetTexture());
	m_shader->SetTextureSampler("material.normalTexture", GL_TEXTURE2, 2, m_textureNormal.GetTexture());
}

void Mesh::BindAttributes()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

	int stride = 8;
	m_elementSize = 8;

	if (m_enableNormalMap)
	{
		stride += 6;
		m_elementSize += 6;
	}

	//1st attribute : vertices
	glEnableVertexAttribArray(m_shader->GetAttrVertices());
	glVertexAttribPointer(m_shader->GetAttrVertices(),
		3,
		GL_FLOAT,
		GL_FALSE,
		stride * sizeof(float),
		(void*)0);

	//2nd attribute : Normals
	glEnableVertexAttribArray(m_shader->GetAttrNormals());
	glVertexAttribPointer(m_shader->GetAttrNormals(),
		3,
		GL_FLOAT,
		GL_FALSE,
		stride * sizeof(float),
		(void*)(3 * sizeof(float)));

	//3rd attribute : texCoords
	glEnableVertexAttribArray(m_shader->GetAttrTexCoords());
	glVertexAttribPointer(m_shader->GetAttrTexCoords(),
		2,
		GL_FLOAT,
		GL_FALSE,
		stride * sizeof(float),
		(void*)(6 * sizeof(float)));

	if (m_enableNormalMap)
	{
		//4th attribute buffer : tangent
		glEnableVertexAttribArray(m_shader->GetAttrTangents());
		glVertexAttribPointer(m_shader->GetAttrTangents(),	//Attr we want to configure
			3,									//Size 3 components
			GL_FLOAT,							//type
			GL_FALSE,							//Normalized?
			stride * sizeof(float),				//stride floats per vertex definition
			(void*)(8 * sizeof(float)));		//array buffer offset

		//5th attribute buffer : bitangent
		glEnableVertexAttribArray(m_shader->GetAttrBitangents());
		glVertexAttribPointer(m_shader->GetAttrBitangents(),
			3,
			GL_FLOAT,
			GL_FALSE,
			stride * sizeof(float),
			(void*)(11 * sizeof(float)));
	}

	//Bind Instancing Data
	if (m_enableInstancing)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_instanceBuffer);

		glEnableVertexAttribArray(m_shader->GetAttrInstanceMatrix());
		glVertexAttribPointer(m_shader->GetAttrInstanceMatrix(),	
			4,									//Size 4 components
			GL_FLOAT,							//type
			GL_FALSE,							//Normalized?
			sizeof(glm::mat4),				//stride
			(void*)0);		//instance buffer offset

		glEnableVertexAttribArray(m_shader->GetAttrInstanceMatrix() + 1);
		glVertexAttribPointer(m_shader->GetAttrInstanceMatrix() + 1,	
			4,									//Size 4 components
			GL_FLOAT,							//type
			GL_FALSE,							//Normalized?
			sizeof(glm::mat4),				//stride
			(void*)(sizeof(glm::vec4)));		//instance buffer offset
		glEnableVertexAttribArray(m_shader->GetAttrInstanceMatrix() + 2);
		glVertexAttribPointer(m_shader->GetAttrInstanceMatrix() + 2,	
			4,									//Size 4 components
			GL_FLOAT,							//type
			GL_FALSE,							//Normalized?
			sizeof(glm::mat4),				//stride
			(void*)(2 * sizeof(glm::vec4)));		//instance buffer offset
		glEnableVertexAttribArray(m_shader->GetAttrInstanceMatrix() + 3);
		glVertexAttribPointer(m_shader->GetAttrInstanceMatrix() + 3,	
			4,									//Size 4 components
			GL_FLOAT,							//type
			GL_FALSE,							//Normalized?
			sizeof(glm::mat4),				//stride
			(void*)(3 * sizeof(glm::vec4)));		//instance buffer offset

		glVertexAttribDivisor(m_shader->GetAttrInstanceMatrix(), 1);
		glVertexAttribDivisor(m_shader->GetAttrInstanceMatrix() + 1, 1);
		glVertexAttribDivisor(m_shader->GetAttrInstanceMatrix() + 2, 1);
		glVertexAttribDivisor(m_shader->GetAttrInstanceMatrix() + 3, 1);


	}

}

string Mesh::Concat(string _s1, int _index, string _s2)
{
	string index = to_string(_index);
	return (_s1 + index + _s2);
}

void Mesh::CalculateTangents(vector<asel::Vertex> _vertices, asel::Vector3& _tangent, asel::Vector3& _bitangent)
{
	asel::Vector3 edge1 = _vertices[1].Position - _vertices[0].Position;
	asel::Vector3 edge2 = _vertices[2].Position - _vertices[0].Position;

	asel::Vector2 deltaUV1 = _vertices[1].TextureCoordinate - _vertices[0].TextureCoordinate;
	asel::Vector2 deltaUV2 = _vertices[2].TextureCoordinate - _vertices[0].TextureCoordinate;

	float f = 1.0f / (deltaUV1.X * deltaUV2.Y - deltaUV2.X * deltaUV1.Y);

	_tangent.X = f * (deltaUV2.Y * edge1.X - deltaUV1.Y * edge2.X);
	_tangent.Y = f * (deltaUV2.Y * edge1.Y - deltaUV1.Y * edge2.Y);
	_tangent.Z = f * (deltaUV2.Y * edge1.Z - deltaUV1.Y * edge2.Z);


	_bitangent.X = f * (-deltaUV2.X * edge1.X + deltaUV1.X * edge2.X);
	_bitangent.Y = f * (-deltaUV2.X * edge1.Y + deltaUV1.X * edge2.Y);
	_bitangent.Z = f * (-deltaUV2.X * edge1.Z + deltaUV1.X * edge2.Z);
}


void Mesh::Render(glm::mat4 _pv, glm::vec3 _specColorVal, float _specStrength)
{
	glUseProgram(m_shader->GetProgramID());

	if (!customRotation)
	{
		m_rotation.x += 0.005f;
	}

	CalculateTransform();
	SetShaderVariables(_pv,_specColorVal,_specStrength);
	BindAttributes();

	if (m_enableInstancing)
	{
		glDrawArraysInstanced(GL_TRIANGLES, 0, m_vertexData.size() / m_elementSize, m_instanceCount);
	}
	else
	{	/*glDrawElements(GL_TRIANGLES, m_indexData.size(), GL_UNSIGNED_BYTE, (void*)0);*/
		glDrawArrays(GL_TRIANGLES, 0, m_vertexData.size() / m_elementSize);
	}

	glDisableVertexAttribArray(m_shader->GetAttrColors());
	glDisableVertexAttribArray(m_shader->GetAttrVertices());
	glDisableVertexAttribArray(m_shader->GetAttrTexCoords());

	if (m_enableNormalMap)
	{
		glDisableVertexAttribArray(m_shader->GetAttrTangents());
		glDisableVertexAttribArray(m_shader->GetAttrBitangents());
	}

	if (m_enableInstancing)
	{
		glDisableVertexAttribArray(m_shader->GetAttrInstanceMatrix());
		glDisableVertexAttribArray(m_shader->GetAttrInstanceMatrix() + 1);
		glDisableVertexAttribArray(m_shader->GetAttrInstanceMatrix() + 2);
		glDisableVertexAttribArray(m_shader->GetAttrInstanceMatrix() + 3);

	}
}



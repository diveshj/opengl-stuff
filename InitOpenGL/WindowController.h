#ifndef WINDOWCONTROLLER_H
#define WINDOWCONTROLLER_H

#include "StandardIncludes.h"

class WindowController : public Singleton<WindowController>
{
public:
	//Contructors / Destructors
	WindowController();
	virtual ~WindowController();

	// Accessors
	GLFWwindow* GetWindow() { if (m_window == nullptr) { NewWindow(); }return m_window; }

	//Methods
	void NewWindow();

	Resolution GetResolution();

private:
	//Members
	GLFWwindow* m_window;
};

#endif //WINDOWCONTROLLER_H
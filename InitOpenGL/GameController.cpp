#include "GameController.h"
#include "WindowController.h"
#include "ToolWindow.h"
#include "Fonts.h"

GameController::GameController()
{
	m_shaderColor = {};
	m_shaderDiffuse = {};
	m_camera = {};
	m_meshBoxes.clear();

}
GameController::~GameController()
{

}
void GameController::Initialize()
{
	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	M_ASSERT(glewInit() == GLEW_OK, "FAILED TO INITIALIZE GLEW");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	srand(time(0));

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	//glFrontFace(GL_CW);

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	m_camera = Camera(WindowController::GetInstance().GetResolution());
}
void GameController::RunGame()
{

	double xpos = 0, ypos = 0;
	InitOpenGL::ToolWindow^ window = gcnew InitOpenGL::ToolWindow();
	window->Show();

	//Creating Shaders here
	m_shaderColor = Shader();
	m_shaderColor.LoadShaders("Color.vertexshader", "Color.fragmentshader");

	m_shaderDiffuse = Shader();
	m_shaderDiffuse.LoadShaders("Diffuse.vertexshader", "Diffuse.fragmentshader");

	m_shaderFont = Shader();
	m_shaderFont.LoadShaders("Font.vertexshader", "Font.fragmentshader");

	m_shaderSkybox = Shader();
	m_shaderSkybox.LoadShaders("Skybox.vertexshader", "Skybox.fragmentshader");

	Shader m_customShader = Shader();
	m_customShader.LoadShaders("custom.vertexshader", "custom.fragmentshader");


	//Light
	Mesh m = Mesh();
	m.Create(&m_shaderColor, "../Assets/Models/sphere.ase");
	m.SetPosition({ 0.0f, 0.0f, 1.5f });
	m.SetColor({ 1.0f,1.0f,1.0f });
	m.SetScale({ 0.005f,0.005f,0.005f });
	Mesh::Lights.push_back(m);


	//Fighter
	Mesh fighter = Mesh();
	fighter.Create(&m_shaderDiffuse, "../Assets/Models/Fighter.ase");
	fighter.SetCameraPosition(m_camera.GetPosition());
	fighter.SetPosition({ 0.0f,0.0f,0.0f });
	fighter.SetScale({ 0.001f,0.001f,0.001f });
	m_meshBoxes.push_back(fighter);


	//Fighter WVP
	Mesh fighterwvp = Mesh();
	fighterwvp.Create(&m_customShader, "../Assets/Models/Fighter.ase");
	fighterwvp.SetCameraPosition(m_camera.GetPosition());
	fighterwvp.SetPosition({ 0.0f,0.0f,0.0f });
	fighterwvp.SetScale({ 0.001f,0.001f,0.001f });
	m_meshBoxes.push_back(fighterwvp);


	////Asteroid instancing
	Mesh asteroid = Mesh();
	asteroid.Create(&m_shaderDiffuse, "../Assets/Models/Asteroid1.ase", 100);
	asteroid.SetCameraPosition(m_camera.GetPosition());
	asteroid.SetPosition({ 0.0f,0.0f,0.0f });
	asteroid.SetScale({ 0.02f,0.02f,0.02f });

	////Skybox
	SkyBox skybox = SkyBox();
	skybox.Create(&m_shaderSkybox, "../Assets/models/skybox.ase",
		{	"../Assets/Textures/Skybox/right.jpg",
			"../Assets/Textures/Skybox/left.jpg",
			"../Assets/Textures/Skybox/top.jpg",
			"../Assets/Textures/Skybox/bottom.jpg",
			"../Assets/Textures/Skybox/front.jpg",
			"../Assets/Textures/Skybox/back.jpg", });


	Fonts f = Fonts();
	f.Create(&m_shaderFont, "arial.ttf", 100);

	double lastTime = glfwGetTime();
	int fps = 0;
	string fpsS = "0";

	glm::vec3 fighterPos;
	glm::vec3 fighterRot;
	glm::vec3 fighterScale;

	GLFWwindow* win = WindowController::GetInstance().GetWindow();
	do
	{

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glm::vec3 specColorval = glm::vec3((float)InitOpenGL::ToolWindow::specValR * 0.01, (float)InitOpenGL::ToolWindow::specValG * 0.01, (float)InitOpenGL::ToolWindow::specValB * 0.01);


#pragma region Handle Move Light

		Mesh::Lights[0].SetRotation(glm::vec3(0.0f, 0.0f, 0.0f));

		if (InitOpenGL::ToolWindow::lightMove)
		{

			int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
			if (state == GLFW_PRESS)
			{
				bool atTop;
				bool atleft;

				xpos < WindowController::GetInstance().GetResolution().m_width * 0.5f ? atleft = true : atleft = false;
				ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

				for (auto& light : Mesh::Lights)
				{
					glm::vec3 lightpos = light.GetPosition();
					lightpos = (lightpos - ((lightpos - (atTop ? glm::vec3(0.0f, 2.0f, 0.0f) : glm::vec3(0.0f, -2.0f, 0.0f))) * 0.01));
					lightpos = (lightpos - ((lightpos - (atleft ? glm::vec3(-2.0f, 0.0f, 0.0f) : glm::vec3(2.0f, 0.0f, 0.0f))) * 0.01));
					light.SetPosition(lightpos);
				}
			}
			//Render Light
			Mesh::Lights[0].Render(m_camera.GetProjection() * m_camera.GetView(), specColorval, (float)InitOpenGL::ToolWindow::specStr);
			//Render Fighter
			m_meshBoxes[0].Render(m_camera.GetProjection() * m_camera.GetView(), specColorval, (float)InitOpenGL::ToolWindow::specStr);
		}
		//Reset Light Position
		if (InitOpenGL::ToolWindow::resetLightPosBool)
		{
			InitOpenGL::ToolWindow::resetLightPosBool = false;
			Mesh::Lights[0].SetPosition({ 0.0f, 0.0f, 1.5f });
		}

#pragma endregion


#pragma region WVP Coloring
		if (InitOpenGL::ToolWindow::colorbyPosBool)
		{

			int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
			if (state == GLFW_PRESS)
			{
				bool atTop;
				bool atleft;

				xpos < WindowController::GetInstance().GetResolution().m_width * 0.5f ? atleft = true : atleft = false;
				ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

				glm::vec3 meshpos = m_meshBoxes[1].GetPosition();
				meshpos = (meshpos - ((meshpos - (atTop ? glm::vec3(0.0f, 2.0f, 0.0f) : glm::vec3(0.0f, -2.0f, 0.0f))) * 0.01));
				meshpos = (meshpos - ((meshpos - (atleft ? glm::vec3(-2.0f, 0.0f, 0.0f) : glm::vec3(2.0f, 0.0f, 0.0f))) * 0.01));
				m_meshBoxes[1].SetPosition(meshpos);
			}
			m_meshBoxes[1].Render(m_camera.GetProjection() * m_camera.GetView(), glm::vec3(3.0f, 3.0f, 3.0f), 4.0f);
		}

		//Reset Mesh Position
		if (InitOpenGL::ToolWindow::resetMeshPosBool)
		{
			InitOpenGL::ToolWindow::resetMeshPosBool = false;
			m_meshBoxes[1].SetPosition({ 0.0f, 0.0f, 0.0f });
		}
#pragma endregion


#pragma region Transform
		if (InitOpenGL::ToolWindow::transformBool)
		{
			m_meshBoxes[0].customRotation = true;


			if (InitOpenGL::ToolWindow::resetTransformBool)
			{
				InitOpenGL::ToolWindow::resetTransformBool = false;
				m_meshBoxes[0].SetPosition({ 0.0f,0.0f,0.0f });
				m_meshBoxes[0].SetScale({ 0.001f,0.001f,0.001f });
				m_meshBoxes[0].SetRotation({ 0.0f,0.0f,0.0f });
			}

			if (InitOpenGL::ToolWindow::translateMeshBool)
			{

				int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
				if (state == GLFW_PRESS)
				{
					bool atTop;
					bool atleft;

					xpos < WindowController::GetInstance().GetResolution().m_width * 0.5f ? atleft = true : atleft = false;
					ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

					glm::vec3 meshpos = m_meshBoxes[0].GetPosition();
					meshpos = (meshpos - ((meshpos - (atTop ? glm::vec3(0.0f, 2.0f, 0.0f) : glm::vec3(0.0f, -2.0f, 0.0f))) * 0.01));
					meshpos = (meshpos - ((meshpos - (atleft ? glm::vec3(-2.0f, 0.0f, 0.0f) : glm::vec3(2.0f, 0.0f, 0.0f))) * 0.01));
					m_meshBoxes[0].SetPosition(meshpos);
				}
				state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE);
				if (state == GLFW_PRESS)
				{
					bool atTop;
					ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

					glm::vec3 meshpos = m_meshBoxes[0].GetPosition();
					meshpos = (meshpos - ((meshpos - (atTop ? glm::vec3(0.0f, 0.0f, 2.0f) : glm::vec3(0.0f, 0.0f, -2.0f))) * 0.01));
					m_meshBoxes[0].SetPosition(meshpos);

				}
			}
			if (InitOpenGL::ToolWindow::rotateMeshBool)
			{

				int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
				if (state == GLFW_PRESS)
				{

					bool atTop;
					bool atleft;

					xpos < WindowController::GetInstance().GetResolution().m_width * 0.5f ? atleft = true : atleft = false;
					ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

					if (atTop && atleft)
					{
						m_meshBoxes[0].Rotate(glm::vec3(0.0f, 2.0f, 0.0f) * 0.01f);
					}
					if (atTop && !atleft)
					{
						m_meshBoxes[0].Rotate(glm::vec3(0.0f, -2.0f, 0.0f) * 0.01f);
					}
					if (!atTop && !atleft)
					{
						m_meshBoxes[0].Rotate(glm::vec3(2.0f, 0.0f, 0.0f) * 0.01f);
					}
					if (!atTop && atleft)
					{
						m_meshBoxes[0].Rotate(glm::vec3(-2.0f, 0.0f, 0.0f) * 0.01f);
					}
				}
				state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE);
				if (state == GLFW_PRESS)
				{
					bool atTop;
					ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

					if (atTop)
					{
						m_meshBoxes[0].Rotate(glm::vec3(0.0f, 0.0f, 2.0f) * 0.01f);
					}
					if (!atTop)

					{
						m_meshBoxes[0].Rotate(glm::vec3(0.0f, 0.0f, -2.0f) * 0.01f);
					}
				}
			}

			if (InitOpenGL::ToolWindow::scaleMeshBool)
			{

				int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
				if (state == GLFW_PRESS)
				{

					bool atTop;
					bool atleft;

					xpos < WindowController::GetInstance().GetResolution().m_width * 0.5f ? atleft = true : atleft = false;
					ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;


					if (atTop)
					{
						m_meshBoxes[0].Scale(glm::vec3(0.0f, 2.0f, 0.0f) * 0.000001f);
					}

					else if (!atTop)
					{
						m_meshBoxes[0].Scale(glm::vec3(0.0f, -2.0f, 0.0f) * 0.000001f);
					}
					if (atleft)
					{
						m_meshBoxes[0].Scale(glm::vec3(-2.0f, 0.0f, 0.0f) * 0.000001f);
					}
					else if (!atleft)
					{
						m_meshBoxes[0].Scale(glm::vec3(2.0f, 0.0f, 0.0f) * 0.000001f);
					}
				}

				state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE);
				if (state == GLFW_PRESS)
				{
					bool atTop;
					ypos < WindowController::GetInstance().GetResolution().m_height * 0.5f ? atTop = true : atTop = false;

					if (atTop)
					{
						m_meshBoxes[0].Scale(glm::vec3(0.0f, 0.0f, 2.0f) * 0.000001f);
					}
					if (!atTop)

					{
						m_meshBoxes[0].Scale(glm::vec3(0.0f, 0.0f, -2.0f) * 0.000001f);
					}
				}

			}
			m_meshBoxes[0].Render(m_camera.GetProjection() * m_camera.GetView(), glm::vec3(3.0f, 3.0f, 3.0f), 4.0f);
		}
#pragma endregion

#pragma region Space Scene

		if (InitOpenGL::ToolWindow::spaceScene)
		{
			//Skybox
			m_camera.Rotate();
			glm::mat4 view = glm::mat4(glm::mat3(m_camera.GetView()));
			skybox.Render(m_camera.GetProjection()* view);
			asteroid.customRotation = true;
			m_meshBoxes[0].SetRotation({ 0.0f,0.0f,0.0f });
			m_meshBoxes[0].Render(m_camera.GetProjection()* m_camera.GetView(), glm::vec3(3.0f, 3.0f, 3.0f), 4.0f);
			asteroid.Render(m_camera.GetProjection()* m_camera.GetView(), glm::vec3(3.0f, 3.0f, 3.0f), 4.0f);
			Mesh::Lights[0].SetLightPosition({ 0.0f,0.0f,0.0f });
		}

#pragma endregion

#pragma region Text Render
		glfwGetCursorPos(WindowController::GetInstance().GetWindow(), &xpos, &ypos);

		double currentTime = glfwGetTime();
		fps++;
		if (currentTime - lastTime >= 1.0)
		{
			fpsS = "FPS: " + to_string(fps);
			fps = 0;
			lastTime += 1.0f;
		}
		f.RenderText(fpsS, 70, 100, 0.5f, { 1.0f,1.0f,1.0f });

		f.RenderText("Mouse Position: " + to_string(xpos) + " " + to_string(ypos), 70, 150, 0.5f, { 1.0f, 1.0f, 0.0f });

		int state2 = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
		if (state2 == GLFW_PRESS)
		{
			f.RenderText("Mouse Left: Down", 70, 200, 0.5f, { 1.0f, 1.0f, 0.0f });
		}
		else
			f.RenderText("Mouse Left: Up", 70, 200, 0.5f, { 1.0f, 1.0f, 0.0f });


		int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE);
		if (state == GLFW_PRESS)
		{
			f.RenderText("Mouse Middle: Down", 70, 250, 0.5f, { 1.0f, 1.0f, 0.0f });
		}
		else
			f.RenderText("Mouse Middle: Up", 70, 250, 0.5f, { 1.0f, 1.0f, 0.0f });

		fighterPos = m_meshBoxes[0].GetPosition();
		f.RenderText("Fighter Position : Vec3(" + to_string(fighterPos.x) + ", " + to_string(fighterPos.y) + ", " + to_string(fighterPos.z), 70, 300, 0.5f, { 1.0f, 1.0f, 0.0f });

		fighterRot = m_meshBoxes[0].GetRotation();
		f.RenderText("Fighter Rotation : Vec3(" + to_string(fighterRot.x) + ", " + to_string(fighterRot.y) + ", " + to_string(fighterRot.z), 70, 350, 0.5f, { 1.0f, 1.0f, 0.0f });

		fighterScale = m_meshBoxes[0].GetScale();
		f.RenderText("Fighter Scale : Vec3(" + to_string(fighterScale.x) + ", " + to_string(fighterScale.y) + ", " + to_string(fighterScale.z), 70, 400, 0.5f, { 1.0f, 1.0f, 0.0f });
#pragma endregion

		System::Windows::Forms::Application::DoEvents();
		glfwSwapBuffers(win);
		glfwPollEvents();
	} while (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(win) == 0);
	for (unsigned int count = 0; count < m_meshBoxes.size(); count++)
	{
		m_meshBoxes[count].Cleanup();
	}

	for (unsigned int count = 0; count < Mesh::Lights.size(); count++)
	{
		Mesh::Lights[count].Cleanup();
	}
	m_skybox.Cleanup();

	m_shaderColor.Cleanup();
	m_shaderDiffuse.Cleanup();
	m_shaderSkybox.Cleanup();
	m_customShader.Cleanup();
}


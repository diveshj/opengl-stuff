#ifndef TEXTURE_H
#define TEXTURE_H
#include "StandardIncludes.h"
class Texture
{
public:
	//constructors/destructor
	Texture();
	virtual ~Texture() {}

	//Accessors
	GLuint GetTexture() { return m_texture; }

	//methods
	void LoadTexture(string _filename);
	void LoadCubemap(vector<std::string> _faces);
	void Cleanup();
	bool EndsWith(const std::string& _str, const std::string& _suffix);

private:
	int m_width;
	int m_height;
	int m_channels;
	GLuint m_texture;
};

#endif //TEXTURE_H
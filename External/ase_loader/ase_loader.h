#ifndef ASE_LOADER
#define ASE_LOADER
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <math.h>

namespace asel
{
#pragma region structs/containers

	struct Vector2
	{
		Vector2()
		{
			X = 0.0f;
			Y = 0.0f;
		}
		Vector2(float X_, float Y_)
		{
			X = X_;
			Y = Y_;
		}
		bool operator==(const Vector2& other) const
		{
			return (this->X == other.X && this->Y == other.Y);
		}
		bool operator!=(const Vector2& other) const
		{
			return !(this->X == other.X && this->Y == other.Y);
		}
		Vector2 operator+(const Vector2& right) const
		{
			return Vector2(this->X + right.X, this->Y + right.Y);
		}
		Vector2 operator-(const Vector2& right) const
		{
			return Vector2(this->X - right.X, this->Y - right.Y);
		}
		Vector2 operator*(const float& other) const
		{
			return Vector2(this->X * other, this->Y * other);
		}
		float X;
		float Y;
	};

	struct Vector3
	{
		Vector3()
		{
			X = 0.0f;
			Y = 0.0f;
			Z = 0.0f;
		}
		Vector3(float X_, float Y_, float Z_)
		{
			X = X_;
			Y = Y_;
			Z = Z_;
		}
		bool operator==(const Vector3& other) const
		{
			return (this->X == other.X && this->Y == other.Y && this->Z == other.Z);
		}
		bool operator!=(const Vector3& other) const
		{
			return !(this->X == other.X && this->Y == other.Y && this->Z == other.Z);
		}
		Vector3 operator+(const Vector3& right) const
		{
			return Vector3(this->X + right.X, this->Y + right.Y, this->Z + right.Z);
		}
		Vector3 operator-(const Vector3& right) const
		{
			return Vector3(this->X - right.X, this->Y - right.Y, this->Z - right.Z);
		}
		Vector3 operator*(const float& other) const
		{
			return Vector3(this->X * other, this->Y * other, this->Z * other);
		}
		Vector3 operator/(const float& other) const
		{
			return Vector3(this->X / other, this->Y / other, this->Z / other);
		}
		float X;
		float Y;
		float Z;
	};

	struct Vertex
	{
		Vector3 Position;
		Vector3 Normal;
		Vector2 TextureCoordinate;
	};

	struct Material
	{
		Material()
		{
			name;
		}
		std::string name;
		Vector3 Kd;
		Vector3 Ks;
		std::string diffuseMap;
		std::string specularMap;
		std::string normalMap;

	};

	struct Mesh
	{
		Mesh()
		{

		}
		Mesh(std::vector<Vertex>& _Vertices)
		{
			Vertices = _Vertices;

		}
		std::string MeshName;
		std::vector<Vertex> Vertices;
		Material MeshMaterial;
	};


#pragma endregion 

#pragma region Algorithm
	namespace algorithm
	{
		class aseAlg
		{
		public:

			static std::string startsWith(const std::string& in)
			{
				if (!in.empty())
				{
					std::string substr;
					size_t token_start = in.find_first_not_of("\t");
					size_t token_end = in.find_first_of(" ", token_start);
					if (token_start != std::string::npos && token_end != std::string::npos)
					{
						substr = in.substr(token_start, token_end - token_start);
						return in.substr(token_start, token_end - token_start);
					}
					else
						return "";
				}
				return "";

			}

			// Split a String into a string array at a given token
			static void split(const std::string& in,
				std::vector<std::string>& out,
				std::string token)
			{
				out.clear();

				std::string temp;

				for (int i = 0; i < int(in.size()); i++)
				{
					std::string test = in.substr(i, token.size());

					if (test == token)
					{
						if (!temp.empty())
						{
							out.push_back(temp);
							temp.clear();
							i += (int)token.size() - 1;
						}
						else
						{
							out.push_back("");
						}
					}
					else if (i + token.size() >= in.size())
					{
						temp += in.substr(i, token.size());
						out.push_back(temp);
						break;
					}
					else
					{
						temp += in[i];
					}
				}
			}

			// Split a String into a string array at a given token
			static void splitforMeshFace(const std::string& in,
				std::vector<std::string>& out,
				std::string token)
			{
				out.clear();

				std::string temp;

				for (int i = 0; i < int(in.size()); i++)
				{
					std::string test = in.substr(i, token.size());

					if (test == token)
					{
						if (!temp.empty())
						{
							out.push_back(temp);
							temp.clear();
							i += (int)token.size() - 1;
						}
						else
						{
							//out.push_back("");
						}
					}
					else if (i + token.size() >= in.size())
					{
						temp += in.substr(i, token.size());
						out.push_back(temp);
						break;
					}
					else
					{
						temp += in[i];
					}
				}
			}


			// Get tail of string after first token and possibly following spaces
			static std::string tail(const std::string& in)
			{
				std::string tail;
				size_t token_start = in.find_first_not_of(" \t");
				size_t space_start = in.find_first_of(" \t", token_start);
				size_t tail_start = in.find_first_not_of(" \t", space_start);
				size_t tail_end = in.find_last_not_of(" \t");
				if (tail_start != std::string::npos && tail_end != std::string::npos)
				{
					tail = in.substr(tail_start, tail_end - tail_start + 1);
					return in.substr(tail_start, tail_end - tail_start + 1);
				}
				else if (tail_start != std::string::npos)
				{
					tail = in.substr(tail_start);
					return in.substr(tail_start);
				}
				return "";
			}

			// Get first token of string
			static std::string firstToken(const std::string& in)
			{
				if (!in.empty())
				{
					size_t token_start = in.find_first_not_of(" \t");
					size_t token_end = in.find_first_of(" \t", token_start);
					if (token_start != std::string::npos && token_end != std::string::npos)
					{
						return in.substr(token_start, token_end - token_start);
					}
					else if (token_start != std::string::npos)
					{
						return in.substr(token_start);
					}
				}
				return "";
			}

		};
	}
#pragma endregion

#pragma region Loader
	class Loader
	{
	public:

		std::vector<asel::Vector3> Mesh_face_Buffer;
		std::vector<asel::Vector3> Mesh_Tface_Buffer;
		std::vector<asel::Vector3> Mesh_faceNormal_Buffer;
		std::vector<Mesh> LoadedMeshes;
		std::vector<Vertex> LoadedVertices;
		std::vector<Material> LoadedMaterials;

		// Default Constructor
		Loader()
		{
			Mesh_face_Buffer.clear();
			Mesh_Tface_Buffer.clear();
			Mesh_faceNormal_Buffer.clear();
		}
		~Loader()
		{
			LoadedVertices.clear();
			LoadedMeshes.clear();
			
		}

		bool LoadFile(std::string Path)
		{
			if (Path.substr(Path.size() - 4, 4) != ".ase")
				return false;


			std::ifstream file(Path);

			if (!file.is_open())
				return false;

			LoadedMeshes.clear();
			LoadedVertices.clear();

			std::vector<Vector3> Positions;
			std::vector<Vector2> TCoords;
			std::vector<Vector3> Normals;
			std::vector<Vertex> Vertices;
			std::vector<std::string> MeshMatNames;
			std::string meshname;
			std::vector<std::string> bitmapNames;
			Material tempMaterial;
			Mesh tempMesh;


			std::string curline;
			while (std::getline(file, curline))
			{
				if (algorithm::aseAlg::startsWith(curline) == "*NODE_NAME")
				{
						std::vector<std::string> spos;
						algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), spos, "\"");
						meshname = spos[1];
				}

				// Generate a Vertex Position
				if (algorithm::aseAlg::startsWith(curline) == "*MESH_VERTEX")
				{
					std::vector<std::string> spos;
					Vector3 vpos;
					algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), spos, "\t");
					vpos.X = std::stof(spos[1]);
					vpos.Y = std::stof(spos[3]);
					vpos.Z = -std::stof(spos[2]);

					Positions.push_back(vpos);
				}
				//Texture Vertex
				if (algorithm::aseAlg::startsWith(curline) == "*MESH_TVERT")
				{
					std::vector<std::string> stex;
					Vector2 vtex;
					algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), stex, "\t");
					vtex.X = std::stof(stex[1]);
					vtex.Y = std::stof(stex[2]);
					TCoords.push_back(vtex);
				}
				//Mesh Faces
				if (algorithm::aseAlg::startsWith(curline) == "*MESH_FACE")
				{
					std::vector<std::string> sface;
					algorithm::aseAlg::splitforMeshFace(algorithm::aseAlg::tail(curline), sface, " ");
					Vector3 vFace;

					vFace.X = std::stof(sface[2]);
					vFace.Y = std::stof(sface[4]);
					vFace.Z = std::stof(sface[6]);
					Mesh_face_Buffer.push_back(vFace);
				}
				//Texture Index
				if (algorithm::aseAlg::startsWith(curline) == "*MESH_TFACE")
				{
					std::vector<std::string> tface;
					algorithm::aseAlg::splitforMeshFace(algorithm::aseAlg::tail(curline), tface, "\t");
					Vector3 tFace;
					tFace.X = std::stof(tface[1]);
					tFace.Y = std::stof(tface[2]);
					tFace.Z = std::stof(tface[3]);
					Mesh_Tface_Buffer.push_back(tFace);
				}
				//Meshnormals
				if (algorithm::aseAlg::startsWith(curline) == "*MESH_FACENORMAL")
				{
					std::vector<std::string> nFace;
					algorithm::aseAlg::splitforMeshFace(algorithm::aseAlg::tail(curline), nFace, "\t");
					Vector3 vnor;
					vnor.X = std::stof(nFace[1]);
					vnor.Y = std::stof(nFace[3]);
					vnor.Z = std::stof(nFace[2]);

					Mesh_faceNormal_Buffer.push_back(vnor);

				}

				if (algorithm::aseAlg::startsWith(curline) == "*MATERIAL_NAME")
				{
					std::vector<std::string> spos;
					algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), spos, "\"");
					tempMaterial.name = algorithm::aseAlg::tail(curline);
				}

				//Store bitmap locations
				if (algorithm::aseAlg::startsWith(curline) == "*BITMAP")
				{
					std::vector<std::string> spos;
					algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), spos, "\"");

					if ((spos[1].find("diffuse") != std::string::npos || spos[1].find("Diffuse") != std::string::npos))
					{
						tempMaterial.diffuseMap = spos[1];
					}
					else if ((spos[1].find("specular") != std::string::npos || spos[1].find("Specular") != std::string::npos))
					{
						tempMaterial.specularMap = spos[1];

					}
					else if ((spos[1].find("normal") != std::string::npos || spos[1].find("Normal") != std::string::npos))
					{
						tempMaterial.normalMap = spos[1];

					}
					else if (tempMaterial.diffuseMap == "")
					{
						tempMaterial.diffuseMap = spos[1];

					}
					else if (tempMaterial.specularMap == "")
					{
						tempMaterial.specularMap = spos[1];

					}
					else if (tempMaterial.normalMap == "")
					{
						tempMaterial.normalMap = spos[1];

					}
				}

				//Diffuse Color
				if (algorithm::aseAlg::startsWith(curline) == "*MATERIAL_DIFFUSE")
				{
					std::vector<std::string> temp;
					algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), temp, "\t");
					tempMaterial.Kd.X = std::stof(temp[0]);
					tempMaterial.Kd.Y = std::stof(temp[1]);
					tempMaterial.Kd.Z = std::stof(temp[2]);
				}
				//Specular Color
				if (algorithm::aseAlg::startsWith(curline) == "*MATERIAL_SPECULAR")
				{
					std::vector<std::string> temp;
					algorithm::aseAlg::split(algorithm::aseAlg::tail(curline), temp, "\t");

					tempMaterial.Ks.X = std::stof(temp[0]);
					tempMaterial.Ks.Y = std::stof(temp[1]);
					tempMaterial.Ks.Z = std::stof(temp[2]);
				}

			}

			//Push Mateerial to be available
			LoadedMaterials.push_back(tempMaterial);

			//Calculate faces and vertices from buffers available
			for (int i = 0; i < Mesh_face_Buffer.size(); i++)
			{
				std::vector<Vertex> vVerts;
				GenerateVerticesfromASE(vVerts, Positions, TCoords, Mesh_face_Buffer, Mesh_Tface_Buffer, Mesh_faceNormal_Buffer, i);

				// Add Vertices
				for (int i = 0; i < int(vVerts.size()); i++)
				{
					Vertices.push_back(vVerts[i]);

					LoadedVertices.push_back(vVerts[i]);
				}
			}

			//If Mesh Generation was successful, push it back 
			if (!Vertices.empty())
			{
				tempMesh = Mesh(Vertices);
				tempMesh.MeshName = meshname;
				LoadedMeshes.push_back(tempMesh);
			}

			//Assign the material.
			if (!LoadedMeshes.empty())
			{
				LoadedMeshes[0].MeshMaterial = LoadedMaterials[0];
			}

			file.close();

			if (LoadedMeshes.empty() && LoadedVertices.empty())
			{
				return false;
			}
			else
			{
				return true;
			}
		}



	private:

		void GenerateVerticesfromASE(std::vector<Vertex>& oVerts,
			const std::vector<Vector3>& iPositions,
			const std::vector<Vector2>& iTCoords,
			std::vector<asel::Vector3>& Mesh_face_Buffer,
			std::vector<asel::Vector3>& Mesh_tface_buffer,
			std::vector<asel::Vector3>& Mesh_faceNorm_buffer, int _i)
		{
			//GenerateVerticesfromASE(vVerts, Positions, TCoords, Mesh_face_Buffer, Mesh_Tface_Buffer, Mesh_faceNormal_Buffer, i);

			Vertex vVert;
			vVert.Position = iPositions[Mesh_face_Buffer[_i].X];
			vVert.TextureCoordinate = iTCoords[Mesh_tface_buffer[_i].X];
			vVert.Normal = Mesh_faceNorm_buffer[_i];
			oVerts.push_back(vVert);

			Vertex vVert2;
			vVert2.Position = iPositions[Mesh_face_Buffer[_i].Y];
			vVert2.TextureCoordinate = iTCoords[Mesh_tface_buffer[_i].Y];
			vVert2.Normal = Mesh_faceNorm_buffer[_i];

			oVerts.push_back(vVert2);

			Vertex vVert3;
			vVert3.Position = iPositions[Mesh_face_Buffer[_i].Z];
			vVert3.TextureCoordinate = iTCoords[Mesh_tface_buffer[_i].Z];
			vVert3.Normal = Mesh_faceNorm_buffer[_i];
			oVerts.push_back(vVert3);


		}
	};

#pragma endregion


}
#endif